#!/bin/bash
SCRIPTNAME="plc_emailer.sh"
logmsg m="$SCRIPTNAME - Running plc monitor script..."
SITENAME=$(mylist TerminalProfile | sed 's/"//g' | awk -F, '{print $3}' | tr -s [:space:])
GROUPNAME="PMON"

fnEmailer() {
  (
    echo -e "Detected PLC running in REMOTE mode at $SITENAME \n"
    echo -e "Device : $2 with IP address : $3 since $4"
  ) > message_body
  mail -s "$SITENAME : PLC BIT STATUS EMAILER" $1 < message_body
  logmsg m="$SCRIPTNAME - Email sent to $1"
}
fnMonitor() {
while read PLC_NAME IP_ADDR EMAIL_CONFIG_WORD EMAIL_CONFIG_BIT STATUS_CONFIG_WORD STATUS_CONFIG_BIT
do
  READ_BIN_VALUE=$(echo "obase=2;$(abplc5read $IP_ADDR $STATUS_CONFIG_WORD | grep Value | cut -d "(" -f2 | cut -d ")" -f1)" | bc)
  logmsg m="$SCRIPTNAME - plc5read obtained a value of $READ_BIN_VALUE"
  if [[ $((${#READ_BIN_VALUE}-1)) == $STATUS_CONFIG_BIT && $READ_BIN_VALUE != 0 ]]; then
    logmsg m="$SCRIPTNAME - Detected first plc remote mode. Logging to plc_emailer.log"
    #tail -c 10000 /tms6/log/plc_emailer.log  > /tms6/log/plc_emailer.log 2>&1
    echo -e "$(date +'%b %d %Y %H:%M:%S %Z')" >> /tms6/log/plc_emailer.log
    WRITE_VALUE=$(echo "ibase=2;$(($READ_BIN_VALUE-$((10**$STATUS_CONFIG_BIT))))" | bc)
    abplc5write $IP_ADDR $STATUS_CONFIG_WORD $WRITE_VALUE
  fi
  READ_BIN_VALUE=$(echo "obase=2;$(abplc5read $IP_ADDR $EMAIL_CONFIG_WORD | grep Value | cut -d "(" -f2 | cut -d ")" -f1)" | bc)
  logmsg m="$SCRIPTNAME - plc5read obtained a value of $READ_BIN_VALUE"
  if [[ $((${#READ_BIN_VALUE}-1)) == $EMAIL_CONFIG_BIT && $READ_BIN_VALUE != 0 ]]; then
    logmsg m="$SCRIPTNAME - Detected email trigger bit"
    WRITE_VALUE=$(echo "ibase=2;$(($READ_BIN_VALUE-$((10**$EMAIL_CONFIG_BIT))))" | bc)
    logmsg m="$SCRIPTNAME - Writing value of $WRITE_VALUE to PLC"
    mylist EmailGroup | grep "$GROUPNAME" | sed 's/"//g' | awk -F, '{print $4 > $3".ema"}'
    email_addr_list=$(awk -vORS=' ' 1 "$GROUPNAME".ema)
    timestamp=$(tail -n 1 /tms6/log/plc_emailer.log)
    #if [[ $PLC_NAME == "L33ER-Dock" ]]; then
    #fnEmailer "$email_addr_list" $PLC_NAME "10.146.85.72" "$timestamp"
    #else
    fnEmailer "$email_addr_list" $PLC_NAME $IP_ADDR "$timestamp"
    #fi
    wait
    abplc5write $IP_ADDR $EMAIL_CONFIG_WORD $WRITE_VALUE
    rm -rf *.ema message_body
  fi
done < /tms6/cfg/plc_emailer.cfg
}

fnMonitor
sleep 30
fnMonitor
#PLC_NAME IP_ADDR EMAIL_CONFIG_WORD EMAIL_CONFIG_BIT STATUS_CONFIG_WORD STATUS_CONFIG_BIT
#L33ER  192.168.100.100 N7:0    1 N7:0    2

#*/1 * * * * . /tms6/scripts/ttcommon_env.sh; /tms6/scripts/plc_emailer.sh