# README #


### What is this repository for? ###
Watches a PLC bit over same subnet and toggles it to zero.

### How do I get set up? ###
1. Copy current script to /tms6/scripts directory.
2. Add a new plc_emailer.cfg file to /tms6/cfg directory.
3. Setup EmailGroup table in tms with the list of emails that need to be emailed. This is the GROUPNAME specified in the config file.
3. Add configuration for plc5 as shown in the script as follows :

`plcName IP_ADDR OPTO_CONFIG_WORD OPTO_CONFIG_BIT GROUPNAME`

`L33R   192.168.100.100 N7:0    1       TEST`

 Setup a crontab to run every minute as 
`*/1 * * * * /tms6/scripts/plc_emailer.sh`
